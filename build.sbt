name := """jobs_aggregator"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test
)

//akka
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-slf4j" % "2.4.0",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4.0"
)

//webjars
libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.4.0-1",
  "org.webjars" % "bootstrap" % "3.3.6",
  "org.webjars" % "jquery" % "2.2.1",
  "org.webjars" % "react" % "0.14.7"
)

//other
libraryDependencies ++= Seq(
  "org.jsoup" % "jsoup" % "1.8.3",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test",
  "org.scalatest" % "scalatest_2.11" % "2.2.4",
  "org.testng" % "testng" % "6.1.1",
  "de.leanovate.play-mockws" %% "play-mockws" % "2.4.2" % "test"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

pipelineStages := Seq(rjs)
