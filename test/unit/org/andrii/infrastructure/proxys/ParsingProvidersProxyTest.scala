package org.andrii.infrastructure.proxys

import akka.actor.ActorSystem
import akka.testkit.TestProbe
import org.andrii.domaine.entitys.{SearchResult, SearchTask, JobsParsingResult, Job}
import org.andrii.domaine.exceptions.MessageTypeMismatchException
import org.andrii.domaine.providers.types.DouProviderType
import org.junit.Test
import org.scalatest.testng.TestNGSuite

/**
  * Created by andrii on 3/3/16.
  */
class ParsingProvidersProxyTest extends TestNGSuite {
  val job = new Job("test", "test", "test", "test")
  val searchTask = SearchTask("")
  implicit val system = ActorSystem("TestSys")
  val p = TestProbe()
  val cut = system.actorOf(ParsingProvidersProxy.props(p.ref))

  @Test
  def testCorrectEmptyResult(): Unit = {
    val douParsingResult = new JobsParsingResult(DouProviderType, searchTask, List())
    p.send(cut, douParsingResult)

    val result = SearchResult(searchTask.keywords, true, Map(), List())

    p.expectMsg(result)
  }

  @Test
  def testCorrectResult(): Unit = {
    val jobs = List(job, job)
    val douParsingResult = new JobsParsingResult(DouProviderType, searchTask, jobs)
    p.send(cut, douParsingResult)

    val result = SearchResult(searchTask.keywords, true, Map(), jobs)

    p.expectMsg(result)
  }

  @Test
  def testThrowable(): Unit = {
    val ex = new Exception()

    p.send(cut, ex)
    p.expectMsg(ex)
  }

  @Test
  def testWrongTypeMessage(): Unit = {
    val str = ""
    p.send(cut, str)
    p.expectMsgType[MessageTypeMismatchException]
  }
}
