package org.andrii.infrastructure.html_parsers

import org.andrii.domaine.entitys.{HttpResponse, SearchTask}
import org.andrii.domaine.providers.types.DouProviderType
import org.junit.Test
import org.scalatest.testng.TestNGSuite
import org.testng.Assert._

/**
  * Created by andrii on 3/4/16.
  */
class DouHtmlParserTest extends TestNGSuite {
  val cut = DouHtmlParser
  val provider = DouProviderType
  val searchTask = SearchTask("test")
  val code = 200

  @Test
  def testOneCorrectJob(): Unit = {
    val body = "<li class=\"l-vacancy\">\n\t<div _id=\"25260\" class=\"vacancy\">\n\t\t<div class=\"title\">\n\t\t\t<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/25260/\" class=\"vt\">Title 1</a>&nbsp;<strong>в\n\t\t\t&nbsp;<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/\" class=\"company\" rel=\"nofollow\"><img src=\"http://s.developers.org.ua/img/static/favicons/softengi.png\" class=\"f-i\">&nbsp;Softengi</a></strong>\t\t\t\t<span class=\"cities\"><em>?</em> Киев</span>\n\t\t</div>\n\t\t\t<div class=\"sh-info\">\n\t\t\t\t Description 1 \t\t\t\t</div>\n\t</div>\n</li>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("Title 1", job.title)
    assertEquals("http://jobs.dou.ua/companies/softengi/vacancies/25260/", job.url)
    assertEquals("Киев", job.city)
    assertEquals("Description 1", job.description)
  }

  @Test
  def testTwoCorrectJob(): Unit = {
    val body = "<ul><li class=\"l-vacancy\">\n\t<div _id=\"25260\" class=\"vacancy\">\n\t\t<div class=\"title\">\n\t\t\t<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/25260/\" class=\"vt\">Title 1</a>&nbsp;<strong>в\n\t\t\t&nbsp;<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/\" class=\"company\" rel=\"nofollow\"><img src=\"http://s.developers.org.ua/img/static/favicons/softengi.png\" class=\"f-i\">&nbsp;Softengi</a></strong>\t\t\t\t<span class=\"cities\"><em>?</em> Киев</span>\n\t\t</div>\n\t\t\t<div class=\"sh-info\">\n\t\t\t\t Description 1 \t\t\t\t</div>\n\t</div>\n</li><li class=\"l-vacancy\">\n\t<div _id=\"25260\" class=\"vacancy\">\n\t\t<div class=\"title\">\n\t\t\t<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/25260/\" class=\"vt\">Title 2</a>&nbsp;<strong>в\n\t\t\t&nbsp;<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/\" class=\"company\" rel=\"nofollow\"><img src=\"http://s.developers.org.ua/img/static/favicons/softengi.png\" class=\"f-i\">&nbsp;Softengi</a></strong>\t\t\t\t<span class=\"cities\"><em>?</em> Киев</span>\n\t\t</div>\n\t\t\t<div class=\"sh-info\">\n\t\t\t\t Description 2 \t\t\t\t</div>\n\t</div>\n</li></ul>"

    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(2, result.size)
    val job1 = result.tail.head
    assertEquals("Title 1", job1.title)
    assertEquals("http://jobs.dou.ua/companies/softengi/vacancies/25260/", job1.url)
    assertEquals("Киев", job1.city)
    assertEquals("Description 1", job1.description)

    val job2 = result.head
    assertEquals("Title 2", job2.title)
    assertEquals("http://jobs.dou.ua/companies/softengi/vacancies/25260/", job2.url)
    assertEquals("Киев", job2.city)
    assertEquals("Description 2", job2.description)
  }

  @Test
  def testOneIncorrectJobWithoutTitle(): Unit = {
    val body = "<li class=\"l-vacancy\">\n\t<div _id=\"25260\" class=\"vacancy\">\n\t\t<div class=\"title\">\n\t\t\t&nbsp;<strong>в\n\t\t\t&nbsp;<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/\" class=\"company\" rel=\"nofollow\"><img src=\"http://s.developers.org.ua/img/static/favicons/softengi.png\" class=\"f-i\">&nbsp;Softengi</a></strong>\t\t\t\t<span class=\"cities\"><em>?</em> Киев</span>\n\t\t</div>\n\t\t\t<div class=\"sh-info\">\n\t\t\t\t Description 1 \t\t\t\t</div>\n\t</div>\n</li>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("", job.title)
    assertEquals("", job.url)
    assertEquals("Киев", job.city)
    assertEquals("Description 1", job.description)
  }

  @Test
  def testOneIncorrectJobWithoutCity(): Unit = {
    val body = "<li class=\"l-vacancy\">\n\t<div _id=\"25260\" class=\"vacancy\">\n\t\t<div class=\"title\">\n\t\t\t<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/25260/\" class=\"vt\">Title 1</a>&nbsp;<strong>в\n\t\t\t&nbsp;<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/\" class=\"company\" rel=\"nofollow\"><img src=\"http://s.developers.org.ua/img/static/favicons/softengi.png\" class=\"f-i\">&nbsp;Softengi</a></strong></div>\n\t\t\t<div class=\"sh-info\">\n\t\t\t\t Description 1 \t\t\t\t</div>\n\t</div>\n</li>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("Title 1", job.title)
    assertEquals("http://jobs.dou.ua/companies/softengi/vacancies/25260/", job.url)
    assertEquals("", job.city)
    assertEquals("Description 1", job.description)
  }

  def testOneIncorrectJobWithoutDescription(): Unit = {
    val body = "<li class=\"l-vacancy\">\n\t<div _id=\"25260\" class=\"vacancy\">\n\t\t<div class=\"title\">\n\t\t\t<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/25260/\" class=\"vt\">Title 1</a>&nbsp;<strong>в\n\t\t\t&nbsp;<a href=\"http://jobs.dou.ua/companies/softengi/vacancies/\" class=\"company\" rel=\"nofollow\"><img src=\"http://s.developers.org.ua/img/static/favicons/softengi.png\" class=\"f-i\">&nbsp;Softengi</a></strong>\t\t\t\t<span class=\"cities\"><em>?</em> Киев</span>\n\t\t</div></div>\n</li>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("Title 1", job.title)
    assertEquals("http://jobs.dou.ua/companies/softengi/vacancies/25260/", job.url)
    assertEquals("Киев", job.city)
    assertEquals("", job.description)
  }

  @Test
  def testEmptyBody(): Unit = {
    val body = ""

    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(0, result.size)
  }

  @Test
  def testWrongBody(): Unit = {
    val body = "<body><div><div></body>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(0, result.size)
  }
}
