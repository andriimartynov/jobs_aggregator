package org.andrii.infrastructure.html_parsers

import org.andrii.domaine.entitys.{HttpResponse, SearchTask}
import org.andrii.domaine.providers.types.WorkUaProviderType
import org.junit.Test
import org.scalatest.testng.TestNGSuite
import org.testng.Assert._

/**
  * Created by andrii on 3/4/16.
  */
class WorkUaHtmlParserTest extends TestNGSuite {
  val cut = WorkUaHtmlParser
  val provider = WorkUaProviderType
  val searchTask = SearchTask("test")
  val code = 200

  @Test
  def testOneCorrectJob(): Unit = {
    val body = "<div class=\"card card-hover card-visited job-link card-logotype\">\n\n<div class=\"logo-img\"><img width=\"100\" height=\"50\" src=\"http://i.work.ua/employer_design/3/4/6/106346_company_logo_1.png\" alt=\"Serena Software\"></div><h2><a title=\"Java Web developer, вакансия от 01.03.16\" href=\"/jobs/2052763/\">Java Web developer</a></h2>\n\n<div>\n<span>Serena Software</span>&nbsp;<span data-toggle=\"popover\" data-content=\"Подлинность компании подтверждена сотрудниками Work.ua. &lt;a href='/help/?id=177' class='bp-more' target='_blank'&gt;Подробнее&lt;span class='glyphicon glyphicon-chevron-right'&gt;&lt;/span&gt;&lt;/a&gt;\" data-original-title=\"\" title=\"\"><span class=\"glyphicon glyphicon-confirmed text-blue-light \"></span></span>&nbsp;<span class=\"text-muted \">· </span><span>Киев<span class=\"text-muted\">&nbsp;·&nbsp;</span><span class=\"label label-hot\">Горячая</span></span></div>\n<p class=\"text-muted overflow\">\nПолная занятость. Опыт работы от 2 лет. Высшее образование.<br>\nSERENA Software (www.serena.com)is&nbsp;the global leader in&nbsp;Application Lifecycle Management for the Enterprise…<a href=\"/jobs/2052763/\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a></p>\n</div>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("Java Web developer", job.title)
    assertEquals("http://www.work.ua/jobs/2052763/", job.url)
    assertEquals("Киев", job.city)
    assertEquals("Полная занятость. Опыт работы от 2 лет. Высшее образование. SERENA Software (www.serena.com)is the global leader in Application Lifecycle Management for the Enterprise…", job.description)
  }

  @Test
  def testTwoCorrectJob(): Unit = {
    val body = "<div><div class=\"card card-hover card-visited job-link card-logotype\">\n\n<div class=\"logo-img\"><img width=\"100\" height=\"50\" src=\"http://i.work.ua/employer_design/3/4/6/106346_company_logo_1.png\" alt=\"Serena Software\"></div><h2><a title=\"Java Web developer, вакансия от 01.03.16\" href=\"/jobs/2052763/\">Java Web developer</a></h2>\n\n<div>\n<span>Serena Software</span>&nbsp;<span data-toggle=\"popover\" data-content=\"Подлинность компании подтверждена сотрудниками Work.ua. &lt;a href='/help/?id=177' class='bp-more' target='_blank'&gt;Подробнее&lt;span class='glyphicon glyphicon-chevron-right'&gt;&lt;/span&gt;&lt;/a&gt;\" data-original-title=\"\" title=\"\"><span class=\"glyphicon glyphicon-confirmed text-blue-light \"></span></span>&nbsp;<span class=\"text-muted \">· </span><span>Киев<span class=\"text-muted\">&nbsp;·&nbsp;</span><span class=\"label label-hot\">Горячая</span></span></div>\n<p class=\"text-muted overflow\">\nПолная занятость. Опыт работы от 2 лет. Высшее образование.<br>\nSERENA Software (www.serena.com)is&nbsp;the global leader in&nbsp;Application Lifecycle Management for the Enterprise…<a href=\"/jobs/2052763/\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a></p>\n</div><a name=\"761806\"></a><div class=\"card card-hover card-visited job-link card-logotype\">\n\n<div class=\"logo-img\"><img width=\"100\" height=\"50\" src=\"http://i.work.ua/employer_design/3/4/6/106346_company_logo_1.png\" alt=\"Serena Software\"></div><h2><a title=\"Java Developer, вакансия от 01.03.16\" href=\"/jobs/761806/\">Java Developer</a></h2>\n\n<div>\n<span>Serena Software</span>&nbsp;<span data-toggle=\"popover\" data-content=\"Подлинность компании подтверждена сотрудниками Work.ua. &lt;a href='/help/?id=177' class='bp-more' target='_blank'&gt;Подробнее&lt;span class='glyphicon glyphicon-chevron-right'&gt;&lt;/span&gt;&lt;/a&gt;\" data-original-title=\"\" title=\"\"><span class=\"glyphicon glyphicon-confirmed text-blue-light \"></span></span>&nbsp;<span class=\"text-muted \">· </span><span>Киев<span class=\"text-muted\">&nbsp;·&nbsp;</span><span class=\"label label-hot\">Горячая</span></span></div>\n<p class=\"text-muted overflow\">\nПолная занятость. Опыт работы от 2 лет. Высшее образование.<br>\nSERENA Software (www.serena.com), global leader in&nbsp;Application Lifecycle Management for the Enterprise…<a href=\"/jobs/761806/\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a></p>\n</div></div>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(2, result.size)
    val job1 = result.tail.head
    assertEquals("Java Web developer", job1.title)
    assertEquals("http://www.work.ua/jobs/2052763/", job1.url)
    assertEquals("Киев", job1.city)
    assertEquals("Полная занятость. Опыт работы от 2 лет. Высшее образование. SERENA Software (www.serena.com)is the global leader in Application Lifecycle Management for the Enterprise…", job1.description)


    val job2 = result.head
    assertEquals("Java Developer", job2.title)
    assertEquals("http://www.work.ua/jobs/761806/", job2.url)
    assertEquals("Киев", job2.city)
    assertEquals("Полная занятость. Опыт работы от 2 лет. Высшее образование. SERENA Software (www.serena.com), global leader in Application Lifecycle Management for the Enterprise…", job2.description)
  }

  @Test
  def testOneIncorrectJobWithoutTitle(): Unit = {
    val body = "<div class=\"card card-hover card-visited job-link card-logotype\">\n\n<div class=\"logo-img\"><img width=\"100\" height=\"50\" src=\"http://i.work.ua/employer_design/3/4/6/106346_company_logo_1.png\" alt=\"Serena Software\"></div><h2></h2>\n\n<div>\n<span>Serena Software</span>&nbsp;<span data-toggle=\"popover\" data-content=\"Подлинность компании подтверждена сотрудниками Work.ua. &lt;a href='/help/?id=177' class='bp-more' target='_blank'&gt;Подробнее&lt;span class='glyphicon glyphicon-chevron-right'&gt;&lt;/span&gt;&lt;/a&gt;\" data-original-title=\"\" title=\"\"><span class=\"glyphicon glyphicon-confirmed text-blue-light \"></span></span>&nbsp;<span class=\"text-muted \">· </span><span>Киев<span class=\"text-muted\">&nbsp;·&nbsp;</span><span class=\"label label-hot\">Горячая</span></span></div>\n<p class=\"text-muted overflow\">\nПолная занятость. Опыт работы от 2 лет. Высшее образование.<br>\nSERENA Software (www.serena.com)is&nbsp;the global leader in&nbsp;Application Lifecycle Management for the Enterprise…<a href=\"/jobs/2052763/\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a></p>\n</div>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("", job.title)
    assertEquals("", job.url)
    assertEquals("Киев", job.city)
    assertEquals("Полная занятость. Опыт работы от 2 лет. Высшее образование. SERENA Software (www.serena.com)is the global leader in Application Lifecycle Management for the Enterprise…", job.description)
  }

  def testOneIncorrectJobWithoutDescription(): Unit = {
    val body = "<div class=\"card card-hover card-visited job-link card-logotype\">\n\n<div class=\"logo-img\"><img width=\"100\" height=\"50\" src=\"http://i.work.ua/employer_design/3/4/6/106346_company_logo_1.png\" alt=\"Serena Software\"></div><h2><a title=\"Java Web developer, вакансия от 01.03.16\" href=\"/jobs/2052763/\">Java Web developer</a></h2>\n\n<div>\n<span>Serena Software</span>&nbsp;<span data-toggle=\"popover\" data-content=\"Подлинность компании подтверждена сотрудниками Work.ua. &lt;a href='/help/?id=177' class='bp-more' target='_blank'&gt;Подробнее&lt;span class='glyphicon glyphicon-chevron-right'&gt;&lt;/span&gt;&lt;/a&gt;\" data-original-title=\"\" title=\"\"><span class=\"glyphicon glyphicon-confirmed text-blue-light \"></span></span>&nbsp;<span class=\"text-muted \">· </span><span>Киев<span class=\"text-muted\">&nbsp;·&nbsp;</span><span class=\"label label-hot\">Горячая</span></span></div></div>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(1, result.size)
    val job = result.head
    assertEquals("Java Web developer", job.title)
    assertEquals("http://www.work.ua/jobs/2052763/", job.url)
    assertEquals("Киев", job.city)
    assertEquals("", job.description)
  }

  @Test
  def testEmptyBody(): Unit = {
    val body = ""

    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(0, result.size)
  }

  @Test
  def testWrongBody(): Unit = {
    val body = "<body><div><div></body>"
    val httpResponse = HttpResponse(provider, searchTask, code, body)
    val result = cut.parse(httpResponse).jobs

    assertEquals(0, result.size)
  }
}
