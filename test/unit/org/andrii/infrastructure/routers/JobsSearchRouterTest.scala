package org.andrii.infrastructure.routers

import akka.actor.{ActorRef, Props, ActorSystem}
import akka.testkit.{TestActors, TestProbe}
import org.andrii.domaine.entitys.{SearchTask}
import org.andrii.domaine.exceptions.MessageTypeMismatchException
import org.andrii.domaine.providers.types.BaseProviderType
import org.andrii.infrastructure.providers.objects.BaseProviderObject
import org.junit.Test
import org.scalatest.testng.TestNGSuite

/**
  * Created by andrii on 3/17/16.
  */

class JobsSearchRouterTest extends TestNGSuite {
  implicit val system = ActorSystem("TestSys")
  val p = TestProbe()

  object TestProviderType extends BaseProviderType {
    def name = "Test"
  }

  object TestBaseProviderObject extends BaseProviderObject {
    def name = TestProviderType.name

    def props = TestActors.echoActorProps

    def workerType = TestProviderType
  }

  trait TestWorkersListTrait extends JobsSearchWorkersListTrait {
    override def workerObjectList = List(TestBaseProviderObject)
  }

  trait TestProxyTrait extends JobsSearchProxyTrait {
    override def proxy(senderRef: ActorRef) = TestActors.forwardActorProps(p.ref)
  }

  val searchTask = SearchTask("")
  val props = Props(new JobsSearchRouter with TestWorkersListTrait with TestProxyTrait)
  val cut = system.actorOf(props)

  @Test
  def testCorrectResult(): Unit = {
    p.send(cut, searchTask)

    p.expectMsg(searchTask)
  }

  @Test
  def testWrongTypeMessage(): Unit = {
    val str = ""
    p.send(cut, str)
    p.expectMsgType[MessageTypeMismatchException]
  }

}
