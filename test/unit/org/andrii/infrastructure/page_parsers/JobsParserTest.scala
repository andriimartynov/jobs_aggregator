package unit.org.andrii.infrastructure.page_parsers

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestProbe
import org.andrii.domaine.entitys._
import org.andrii.domaine.exceptions.{HtmlParserNotExistException, HttpResponseUnsuccsessException, MessageTypeMismatchException}
import org.andrii.domaine.html_parsers.BaseHtmlParser
import org.andrii.domaine.providers.types.DouProviderType
import org.andrii.infrastructure.html_parsers.{HtmlParserFactory, JobsParser, JobsParserTrait}
import org.junit.Test
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

/**
  * Created by andrii on 3/17/16.
  */
class JobsParserTest extends FlatSpec with MockFactory {
  val pageParsingResult = PageParsingResult(1, List())

  trait TestJobsParserTrait extends JobsParserTrait {
    override def htmlParserFactory = htmlParserFactoryMock
  }

  class TestHtmlParser extends BaseHtmlParser {
    def parse(httpResponse: HttpResponse) = pageParsingResult
  }

  val htmlParserFactoryMock = stub[HtmlParserFactory]
  val htmlParser = stub[TestHtmlParser]


  implicit val system = ActorSystem("TestSys")
  val p = TestProbe()

  val props = Props(new JobsParser(p.ref) with TestJobsParserTrait)
  val cut = system.actorOf(props)
  val searchTask = SearchTask("")


  @Test
  def testCorrectFirstPageOnePageResult(): Unit = {
    val httpResponse = HttpResponse(DouProviderType, searchTask, 200, "")
    val result = JobsParsingResult(httpResponse, pageParsingResult)

    (htmlParserFactoryMock.getHtmlParser _).when(httpResponse).returns(Option(htmlParser))
    (htmlParser.parse _).when(httpResponse).returns(pageParsingResult)


    p.send(cut, httpResponse)
    p.expectMsg(result)
  }

  @Test
  def testCorrectPageOneTwoPageResult(): Unit = {
    val pageParsingResult = PageParsingResult(2, List())
    val httpResponse = HttpResponse(DouProviderType, searchTask, 200, "")
    val result = JobsParsingResult(httpResponse, pageParsingResult)
    val newSearchTask = SearchTask(searchTask.keywords, 2)

    (htmlParserFactoryMock.getHtmlParser _).when(httpResponse).returns(Option(htmlParser))
    (htmlParser.parse _).when(httpResponse).returns(pageParsingResult)


    p.send(cut, httpResponse)
    p.expectMsg(result)
    p.expectMsg(newSearchTask)
  }

  @Test
  def testCorrectSecondPageOnePageResult(): Unit = {
    val searchTask = SearchTask("", 2)
    val httpResponse = HttpResponse(DouProviderType, searchTask, 200, "")
    val result = JobsParsingResult(httpResponse, pageParsingResult)

    (htmlParserFactoryMock.getHtmlParser _).when(httpResponse).returns(Option(htmlParser))
    (htmlParser.parse _).when(httpResponse).returns(pageParsingResult)


    p.send(cut, httpResponse)
    p.expectMsg(result)
  }

  @Test
  def testCorrectSecondPageTwoPageResult(): Unit = {
    val searchTask = SearchTask("", 2)
    val pageParsingResult = PageParsingResult(2, List())
    val httpResponse = HttpResponse(DouProviderType, searchTask, 200, "")
    val result = JobsParsingResult(httpResponse, pageParsingResult)

    (htmlParserFactoryMock.getHtmlParser _).when(httpResponse).returns(Option(htmlParser))
    (htmlParser.parse _).when(httpResponse).returns(pageParsingResult)


    p.send(cut, httpResponse)
    p.expectMsg(result)
  }

  @Test
  def testResponseUnsuccsess(): Unit = {
    val httpResponse = HttpResponse(DouProviderType, searchTask, 300, "")
    p.send(cut, httpResponse)
    p.expectMsgType[HttpResponseUnsuccsessException]
  }

  @Test
  def testParserNotExist(): Unit = {
    val httpResponse = HttpResponse(DouProviderType, searchTask, 200, "")
    (htmlParserFactoryMock.getHtmlParser _).when(httpResponse).returns(None)

    p.send(cut, httpResponse)
    p.expectMsgType[HtmlParserNotExistException]
  }

  @Test
  def testWrongTypeMessage(): Unit = {
    val str = ""
    p.send(cut, str)
    p.expectMsgType[MessageTypeMismatchException]
  }

}
