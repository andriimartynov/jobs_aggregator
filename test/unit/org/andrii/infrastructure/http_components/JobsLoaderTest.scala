package org.andrii.infrastructure.http_components

import akka.actor.{Props, ActorSystem, ActorRef}
import akka.testkit.{TestProbe, TestActors}
import mockws.{Route, MockWS}
import org.andrii.domaine.entitys.{HttpResponse, HttpRequestParams, JobsLoadTask, SearchTask}
import org.andrii.domaine.exceptions.{UrlBuilderNotExistException, MessageTypeMismatchException}
import org.andrii.domaine.http_components.{BaseUrlBuilder, UrlBuilderFactory}
import org.andrii.domaine.providers.types.DouProviderType
import org.andrii.infrastructure.page_parsers.{JobsLoaderTrait, JobsLoader}
import org.junit.Test
import org.scalatest.FlatSpec
import org.scalamock.scalatest.MockFactory
import play.api.mvc.Action
import play.api.mvc.Results._
import play.api.test.Helpers._

/**
  * Created by andrii on 3/17/16.
  */
class JobsLoaderTest extends FlatSpec with MockFactory {

  class TestUrlBuilder extends BaseUrlBuilder {
    def baseUrl = ""
  }

  val route1 = Route {
    case (GET, "/test1") => Action { Ok("body") }
  }

  val ningWSClientMock = MockWS (route1)

  val testUrlBuilderMock = mock[TestUrlBuilder]
  val urlBuilderFactoryMock = stub[UrlBuilderFactory]

  trait TestJobsLoaderTrait extends JobsLoaderTrait{
    override def httpClient = ningWSClientMock

    override def props(provider: ActorRef) = TestActors.echoActorProps

    override def urlBuilderFactory = urlBuilderFactoryMock
  }

  implicit val system = ActorSystem("TestSys")
  val p = TestProbe()

  val props = Props(new JobsLoader(p.ref) with TestJobsLoaderTrait)
  val cut = system.actorOf(props)
  val searchTask = SearchTask("")
  val jobsLoadTask = JobsLoadTask(DouProviderType, searchTask)
  val httpRequest = HttpRequestParams("/test1", Seq(("test", "test")))


  @Test
  def testCorrect(): Unit = {
    val result = HttpResponse(jobsLoadTask.provider, jobsLoadTask.searchTask, 200, "body")
    (urlBuilderFactoryMock.getUrlBuilder _).when(jobsLoadTask).returns(Option(testUrlBuilderMock))
    (testUrlBuilderMock.build _).expects(jobsLoadTask.searchTask).returns(httpRequest)

    p.send(cut, jobsLoadTask)
    p.expectMsg(result)
  }

  @Test
  def testUrlBuilderNotExist(): Unit = {
    (urlBuilderFactoryMock.getUrlBuilder _).when(jobsLoadTask).returns(None)

    p.send(cut, jobsLoadTask)
    p.expectMsgType[UrlBuilderNotExistException]
  }

  @Test
  def testWrongTypeMessage(): Unit = {
    val str = ""
    p.send(cut, str)
    p.expectMsgType[MessageTypeMismatchException]
  }
}
