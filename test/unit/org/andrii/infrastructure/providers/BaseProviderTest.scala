package org.andrii.infrastructure.providers

import akka.actor.{Props, ActorSystem, ActorRef}
import akka.testkit.{TestProbe, TestActors}
import org.andrii.domaine.entitys.{JobsLoadTask, SearchTask}
import org.andrii.domaine.exceptions.MessageTypeMismatchException
import org.andrii.domaine.providers.types.DouProviderType
import org.junit.Test

/**
  * Created by andrii on 3/17/16.
  */
class BaseProviderTest {
  trait TestProviderTrait extends DouProviderTrait {
    override def poolSize = 1

    override def props(provider: ActorRef) = TestActors.echoActorProps
  }

  implicit val system = ActorSystem("TestSys")
  val p = TestProbe()
  val searchTask = SearchTask("")
  val props = Props(new DouProvider with TestProviderTrait)
  val cut = system.actorOf(props)

  @Test
  def testCorrectResult(): Unit = {
    val result = JobsLoadTask(DouProviderType, searchTask)
    p.send(cut, searchTask)

    p.expectMsg(result)
  }

  @Test
  def testWrongTypeMessage(): Unit = {
    val str = ""
    p.send(cut, str)
    p.expectMsgType[MessageTypeMismatchException]
  }
}
