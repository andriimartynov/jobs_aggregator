package org.andrii.infrastructure.web_sockets

import akka.actor.{Props, ActorSystem}
import akka.testkit.TestProbe
import org.andrii.domaine.entitys.{SearchTask, SearchResult}
import org.andrii.infrastructure.i18n.Messages
import org.andrii.infrastructure.loggers.PlayLogger
import org.junit.Test
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec
import org.testng.Assert._
import play.api.libs.json.Json

/**
  * Created by andrii on 3/18/16.
  */
class SearchWebSocketActorTest extends FlatSpec with MockFactory {

  val messagesMokedFunction = mockFunction[String, Seq[Any], String]

  val errorOneArgumentsMoked = mockFunction[String, Unit]
  val errorTwoArgumentsMoked = mockFunction[String, Throwable, Unit]

  errorOneArgumentsMoked.expects(*).anyNumberOfTimes()

  trait TestMessages extends Messages {
    override def messages(keys: String, args: Seq[Any]) = messagesMokedFunction(keys, args)
  }

  trait TestLogger extends PlayLogger {
    override def debug(message: String): Unit = errorOneArgumentsMoked(message)

    override def error(message: String): Unit = errorOneArgumentsMoked(message)

    override def error(message: String, error: Throwable): Unit = errorTwoArgumentsMoked(message, error)
  }


  implicit val system = ActorSystem("TestSys")
  val p = TestProbe()

  val props = Props(new SearchWebSocketActor(p.ref, p.ref) with TestMessages with TestLogger)
  val cut = system.actorOf(props)

  @Test
  def testCorrectSearchRequest(): Unit = {
    val json = Json.obj("keywords" -> "test")
    val searchTask = SearchTask("test")
    p.send(cut, json)
    p.expectMsg(searchTask)
  }

  @Test
  def testEmptyKewords(): Unit = {
    messagesMokedFunction.expects("error.minLength", Seq(3)).returns("test")

    val json = Json.obj("keywords" -> "")
    val searchResult = SearchResult("", false, Map("keywords" -> "test"), List())
    p.send(cut, json)
    p.expectMsg(searchResult)
  }

  @Test
  def testShortKewords(): Unit = {
    messagesMokedFunction.expects("error.minLength", Seq(3)).returns("test")

    val json = Json.obj("keywords" -> "ts")
    val searchResult = SearchResult("", false, Map("keywords" -> "test"), List())
    p.send(cut, json)
    p.expectMsg(searchResult)
  }

  @Test
  def testSearchResult(): Unit = {
    val searchResult = SearchResult("", true, Map(), List())
    p.send(cut, searchResult)
    p.expectMsg(searchResult)
  }

  @Test
  def testThrowable(): Unit = {
    val ex = new Exception("test")
    val a = errorTwoArgumentsMoked.expects(ex.getMessage, ex)
    p.send(cut, ex)
    p.expectNoMsg()

    //TODO: do something with it!!!
    assertEquals("MockFunction2-3(test, java.lang.Exception: test) once (called once)", a.times().toString(), "Logger never called")
  }
}
