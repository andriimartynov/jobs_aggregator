package org.andrii.domaine.http_components

import org.andrii.domaine.entitys.SearchTask
import org.junit.Test
import org.testng.Assert._
import org.scalatest.testng.TestNGSuite

/**
  * Created by andrii on 3/3/16.
  */
class DouUrlBuilderTest extends TestNGSuite {

  val cut = DouUrlBuilder()

  @Test
  def testEmptyKeywords(): Unit = {
    val searchTask = SearchTask("")

    val result = cut.build(searchTask)
    assertEquals("http://jobs.dou.ua/vacancies/", result.url)
    assertEquals(Seq(("search", "")), result.queryMap)
  }

  @Test
  def testOneWord(): Unit = {
    val searchTask = SearchTask("java")

    val result = cut.build(searchTask)
    assertEquals("http://jobs.dou.ua/vacancies/", result.url)
    assertEquals(Seq(("search", "java")), result.queryMap)
  }

  @Test
  def testTwoWord(): Unit = {
    val searchTask = SearchTask("java developer")

    val result = cut.build(searchTask)
    assertEquals("http://jobs.dou.ua/vacancies/", result.url)
    assertEquals(Seq(("search", "java developer")), result.queryMap)
  }

}
