package org.andrii.domaine.html_parsers

import org.andrii.domaine.entitys.SearchTask
import org.andrii.domaine.http_components.WorkUaUrlBuilder
import org.junit.Test
import org.scalatest.testng.TestNGSuite
import org.testng.Assert._

/**
  * Created by andrii on 3/3/16.
  */
class WorkUaUrlBuilderTest extends TestNGSuite {

  val cut = WorkUaUrlBuilder()

  @Test
  def testEmptyKeywords(): Unit = {
    val searchTask = SearchTask("")

    val result = cut.build(searchTask)
    assertEquals("http://www.work.ua/jobs-", result.url)
    assertEquals(Seq(("page", "1")), result.queryMap)
  }

  @Test
  def testOneWord(): Unit = {
    val searchTask = SearchTask("java")

    val result = cut.build(searchTask)
    assertEquals("http://www.work.ua/jobs-java", result.url)
    assertEquals(Seq(("page", "1")), result.queryMap)
  }

  @Test
  def testOneWordPageTwo(): Unit = {
    val searchTask = SearchTask("java", 2)

    val result = cut.build(searchTask)
    assertEquals("http://www.work.ua/jobs-java", result.url)
    assertEquals(Seq(("page", "2")), result.queryMap)
  }

  @Test
  def testOneWordPageThree(): Unit = {
    val searchTask = SearchTask("java", 3)

    val result = cut.build(searchTask)
    assertEquals("http://www.work.ua/jobs-java", result.url)
    assertEquals(Seq(("page", "3")), result.queryMap)
  }

  @Test
  def testTwoWord(): Unit = {
    val searchTask = SearchTask("java developer")

    val result = cut.build(searchTask)
    assertEquals("http://www.work.ua/jobs-java+developer", result.url)
    assertEquals(Seq(("page", "1")), result.queryMap)
  }

}
