package org.andrii.infrastructure.web_sockets

import akka.actor.{Props, PoisonPill, Actor, ActorRef}
import org.andrii.domaine.entitys.{SearchTask, SearchResult, SearchForm}
import org.andrii.infrastructure.loggers.PlayLogger
import org.andrii.infrastructure.i18n.Messages
import play.api.data.Form
import play.api.libs.json.JsValue

/**
  * Created by andrii on 3/11/16.
  */
object SearchWebSocketActor {
  def props(out: ActorRef, jobSearchRouter: ActorRef) = Props(new SearchWebSocketActor(out, jobSearchRouter))
}

class SearchWebSocketActor(out: ActorRef, jobSearchRouter: ActorRef) extends Actor with Messages with PlayLogger {

  private def executeSearchRequest(searchForm: SearchForm) = {
    val searchTask = SearchTask(searchForm)
    jobSearchRouter ! searchTask
  }

  override def postStop() = {
    debug("wsWithActor, client disconnected")
    self ! PoisonPill
  }

  private def receiveSearchRequest(searchForm: JsValue) = {
    SearchForm.form.bind(searchForm).fold(
      errorForm => returnError(errorForm),
      searchForm => executeSearchRequest(searchForm)
    )
  }

  def receive = {
    case searchForm: JsValue =>
      debug(s"actor, received find job message: $searchForm")
      receiveSearchRequest(searchForm)
    case searchResult: SearchResult =>
      out ! searchResult
    case exception: Throwable =>
      val message = exception.getMessage
      error(message, exception)
  }

  private def returnError(errorForm: Form[SearchForm]) = {
    val errors = SearchForm.getErrorsMessages(errorForm, messages)
    out ! SearchResult("", false, errors, List())
  }
}
