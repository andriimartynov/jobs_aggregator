package org.andrii.infrastructure.i18n

import play.api.Play.current
import play.api.i18n.Messages.Implicits._
import play.api.i18n.Messages

/**
  * Created by andrii on 3/20/16.
  */
trait Messages {
  def messages(keys: String, args: Seq[Any]): String = {
    Messages(keys, args: _*)
  }

  def messages(keys: Seq[String], args: Seq[Any]): String = {
    Messages(keys, args: _*)
  }
}
