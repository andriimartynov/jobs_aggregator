package org.andrii.infrastructure.html_parsers

import org.andrii.domaine.entitys.{HttpResponse}
import org.andrii.domaine.html_parsers.BaseHtmlParser
import org.andrii.domaine.providers.types.{WorkUaProviderType, DouProviderType}

/**
  * Created by andrii on 3/4/16.
  */
class HtmlParserFactory {
  val douHtmlParser = DouHtmlParser
  val workUaHtmlParser = WorkUaHtmlParser

  def getHtmlParser(httpResponse: HttpResponse): Option[BaseHtmlParser] = {
    httpResponse.provider match {
      case DouProviderType => Some(douHtmlParser)
      case WorkUaProviderType => Some(workUaHtmlParser)
      case _ => None
    }
  }
}
