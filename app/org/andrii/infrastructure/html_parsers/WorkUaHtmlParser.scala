package org.andrii.infrastructure.html_parsers

import org.andrii.domaine.entitys.{PageParsingResult, HttpResponse, Job}
import org.andrii.domaine.providers.types.BaseProviderType
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

/**
  * Created by andrii on 3/4/16.
  */
object WorkUaHtmlParser extends JsoupHtmlParser {
  val jobSelector = ".job-link"
  val linkSelector = "h2 > a"
  val citySelector = "div > span"
  val descriptionSelector = "p.text-muted.overflow"
  val paginationSelector = ".pagination.hidden-xs li"

  protected def parseElement(element: Element, provider: BaseProviderType): Job = {
    val titleElement = element.select(linkSelector).first()
    val title = if (titleElement != null) titleElement.html().trim
    else ""
    val url = if (titleElement != null) "http://www.work.ua" + titleElement.attr("href").trim
    else ""

    val cityElement = element.select(citySelector).last()
    val city = if (cityElement != null) {
      cityElement.select("span").remove()
      cityElement.text().trim
    } else ""


    val descriptionElement = element.select(descriptionSelector).first()
    val description = if (descriptionElement != null) descriptionElement.text().trim
    else ""

    Job(title, url, city, description)
  }

  protected def getPages(elements: Elements): Int = {
    try {
      if (!elements.isEmpty) {
        elements.remove(elements.size() - 1)
        val link = elements.last()
        return link.children().html().toInt
      }
    } catch {
      case ex: Throwable => return 1
    }

    1
  }

  def parse(httpResponse: HttpResponse) = {
    val doc = Jsoup.parse(httpResponse.body)
    val jobElements = doc.select(jobSelector)
    val jobs = parseElements(jobElements, httpResponse.provider)
    val pagination = doc.select(paginationSelector)
    val pages = if (httpResponse.searchTask.page == 1)
      getPages(pagination)
    else 1
    PageParsingResult(pages, jobs)
  }
}