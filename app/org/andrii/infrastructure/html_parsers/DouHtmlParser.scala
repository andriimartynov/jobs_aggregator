package org.andrii.infrastructure.html_parsers

import org.andrii.domaine.entitys.{PageParsingResult, HttpResponse, Job}
import org.andrii.domaine.providers.types.BaseProviderType
import org.andrii.infrastructure.html_parsers.WorkUaHtmlParser._
import org.jsoup.Jsoup
import org.jsoup.nodes.Element

/**
  * Created by andrii on 3/4/16.
  */
object DouHtmlParser extends JsoupHtmlParser {
  val jobSelector = ".l-vacancy"
  val linkSelector = "a.vt"
  val citySelector = ".cities"
  val descriptionSelector = ".sh-info"

  protected def parseElement(element: Element, provider: BaseProviderType): Job = {
    val titleElement = element.select(linkSelector).first()
    val title = if (titleElement != null) titleElement.html().trim
    else ""
    val url = if (titleElement != null) titleElement.attr("href").trim
    else ""

    val cityElement = element.select(citySelector).first()
    val city = if (cityElement != null) {
      cityElement.select("em").remove()
      cityElement.text().trim
    } else ""


    val descriptionElement = element.select(descriptionSelector).first()
    val description = if (descriptionElement != null) descriptionElement.text().trim
    else ""

    Job(title, url, city, description)
  }

  def parse(httpResponse: HttpResponse) = {
    val doc = Jsoup.parse(httpResponse.body)
    val jobElements = doc.select(jobSelector)
    val jobs = parseElements(jobElements, httpResponse.provider)
    PageParsingResult(1, jobs)
  }
}
