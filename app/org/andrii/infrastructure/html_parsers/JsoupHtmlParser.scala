package org.andrii.infrastructure.html_parsers

import org.andrii.domaine.entitys.Job
import org.andrii.domaine.html_parsers.BaseHtmlParser
import org.andrii.domaine.providers.types.BaseProviderType
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

/**
  * Created by andrii on 3/4/16.
  */
abstract class JsoupHtmlParser extends BaseHtmlParser {
  protected def iterateElements(iterator: java.util.Iterator[Element], result: List[Job], provider: BaseProviderType): List[Job] = {
    if (!iterator.hasNext) return result
    val job = parseElement(iterator.next(), provider)

    iterateElements(iterator, job :: result, provider)
  }

  protected def parseElement(element: Element, provider: BaseProviderType): Job

  protected def parseElements(elements: Elements, provider: BaseProviderType): List[Job] = {
    iterateElements(elements.iterator(), Nil, provider)
  }
}
