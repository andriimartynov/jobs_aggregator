package org.andrii.infrastructure.html_parsers

import akka.actor.{Props, ActorRef, Actor}
import org.andrii.domaine.entitys.{PageParsingResult, SearchTask, JobsParsingResult, HttpResponse}
import org.andrii.domaine.exceptions.{HttpResponseUnsuccsessException, MessageTypeMismatchException, HtmlParserNotExistException}

/**
  * Created by andrii on 3/3/16.
  */

trait JobsParserTrait {
  def htmlParserFactory = new HtmlParserFactory
}

object JobsParser {
  def apply(provider: ActorRef) = new JobsParser(provider)

  def props(provider: ActorRef) = Props(JobsParser(provider))
}


class JobsParser(provider: ActorRef) extends Actor with JobsParserTrait {

  private def getParser(response: HttpResponse) = {
    htmlParserFactory.getHtmlParser(response)
  }

  private def loadFromOtherPages(searchTask: SearchTask, pageParsingResult: PageParsingResult) = {
    if (searchTask.page == 1 && pageParsingResult.pages > 1) {
      val senderRef = sender()
      (2 to pageParsingResult.pages)
        .map(SearchTask(searchTask.keywords, _))
        .foreach(fj => provider.tell(fj, senderRef))
    }
  }

  private def parse(response: HttpResponse): Unit = {
    val option = getParser(response)

    option match {
      case Some(htmlParser) =>
        val pageParsingResult = htmlParser.parse(response)
        val result = JobsParsingResult(response, pageParsingResult)
        sender() ! result
        loadFromOtherPages(response.searchTask, pageParsingResult)
      case None =>
        sender() ! HtmlParserNotExistException(response)
    }
  }

  def receive = {
    case response: HttpResponse =>
      response.isSuccsess() match {
        case true =>
          parse(response)
        case false =>
          sender() ! HttpResponseUnsuccsessException(response)
      }
    case _ =>
      sender() ! MessageTypeMismatchException(sender(), self)
  }
}
