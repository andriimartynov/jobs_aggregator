package org.andrii.infrastructure.routers

import akka.actor.ActorRef
import org.andrii.domaine.entitys.SearchTask
import org.andrii.domaine.exceptions.MessageTypeMismatchException

/**
  * Created by andrii on 3/2/16.
  */
abstract class BaseBroadcastGroupWithProxyRouter extends BaseBroadcastGroupRouter with BaseProxyTrait {

  private def buildProxy(senderRef: ActorRef): ActorRef =
    context.actorOf(proxy(senderRef))

  override def receive = {
    case searchTask: SearchTask =>
      val senderRef = sender()
      val providersProxy = buildProxy(senderRef)
      router.tell(searchTask, providersProxy)
    case _ =>
      sender() ! MessageTypeMismatchException(sender(), self)
  }
}