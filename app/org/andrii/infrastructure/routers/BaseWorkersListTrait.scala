package org.andrii.infrastructure.routers

import org.andrii.infrastructure.providers.objects.BaseProviderObject

/**
  * Created by andrii on 3/17/16.
  */
trait BaseWorkersListTrait {
  def workerObjectList: List[BaseProviderObject]
}
