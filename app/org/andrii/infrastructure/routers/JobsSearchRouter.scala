package org.andrii.infrastructure.routers

import akka.actor.{Props, ActorRef}
import org.andrii.infrastructure.providers.objects.BaseProviderObject
import org.andrii.infrastructure.proxys.ParsingProvidersProxy
import org.andrii.infrastructure.providers.ProviderList

/**
  * Created by andrii on 3/3/16.
  */

trait JobsSearchWorkersListTrait extends BaseWorkersListTrait {
  def workerObjectList: List[BaseProviderObject] = ProviderList.list
}

trait JobsSearchProxyTrait extends BaseProxyTrait {
  def proxy(senderRef: ActorRef) = ParsingProvidersProxy.props(senderRef)
}

object JobsSearchRouter {
  def name = "jobs-search-router"

  def props = Props[JobsSearchRouter]
}

class JobsSearchRouter extends BaseBroadcastGroupWithProxyRouter with JobsSearchWorkersListTrait with JobsSearchProxyTrait
