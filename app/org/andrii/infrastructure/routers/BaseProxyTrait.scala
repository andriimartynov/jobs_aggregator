package org.andrii.infrastructure.routers

import akka.actor.{Props, ActorRef}

/**
  * Created by andrii on 3/17/16.
  */
trait BaseProxyTrait {
  def proxy(senderRef: ActorRef): Props
}
