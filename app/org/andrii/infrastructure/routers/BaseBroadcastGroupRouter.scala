package org.andrii.infrastructure.routers

import akka.actor.Actor
import akka.routing.BroadcastGroup
import org.andrii.domaine.entitys.SearchTask
import org.andrii.domaine.exceptions.MessageTypeMismatchException

/**
  * Created by andrii on 3/2/16.
  */
abstract class BaseBroadcastGroupRouter extends Actor with BaseWorkersListTrait {

  def paths: List[String] = workerObjectList.map(workerObj => self.path + "/" + workerObj.name)

  workerObjectList.foreach(workerObj =>
    context.actorOf(workerObj.props, workerObj.name)
  )

  val router =
    context.actorOf(BroadcastGroup(paths).props(), "router")

  def receive = {
    case searchTask: SearchTask =>
      val senderRef = sender()
      router.tell(SearchTask, senderRef)
    case _ =>
      sender() ! MessageTypeMismatchException(sender(), self)
  }
}