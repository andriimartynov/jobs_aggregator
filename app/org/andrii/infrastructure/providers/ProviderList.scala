package org.andrii.infrastructure.providers

import org.andrii.domaine.providers.types.BaseProviderType
import org.andrii.infrastructure.providers.objects.BaseProviderObject

/**
  * Created by andrii on 3/3/16.
  */
object ProviderList {
  def list: List[BaseProviderObject] = List(DouProviderObject, WorkuaProviderObject)

  def map: Map[BaseProviderType, Int] = list.map(worker => (worker.workerType, 1)).toMap
}
