package org.andrii.infrastructure.providers

import akka.actor.Props
import org.andrii.domaine.providers.types.DouProviderType
import org.andrii.infrastructure.providers.objects.BaseProviderObject

/**
  * Created by andrii on 3/2/16.
  */
object DouProviderObject extends BaseProviderObject {
  def name: String = DouProviderType.name

  def props = Props[DouProvider]

  def workerType = DouProviderType
}
