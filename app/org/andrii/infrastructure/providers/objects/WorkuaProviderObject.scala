package org.andrii.infrastructure.providers

import akka.actor.Props
import org.andrii.domaine.providers.types.WorkUaProviderType
import org.andrii.infrastructure.providers.objects.BaseProviderObject

/**
  * Created by andrii on 3/2/16.
  */
object WorkuaProviderObject extends BaseProviderObject {
  def name: String = WorkUaProviderType.name

  def props = Props[WorkUaProvider]

  def workerType = WorkUaProviderType
}
