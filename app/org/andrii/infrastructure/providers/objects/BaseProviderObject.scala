package org.andrii.infrastructure.providers.objects

import akka.actor.Props
import org.andrii.domaine.providers.types.BaseProviderType

/**
  * Created by andrii on 3/2/16.
  */
abstract class BaseProviderObject {
  def name: String

  def props: Props

  def workerType: BaseProviderType
}
