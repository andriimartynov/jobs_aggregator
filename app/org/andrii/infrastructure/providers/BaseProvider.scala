package org.andrii.infrastructure.providers

import akka.actor.{ActorRef, Props, Actor}
import akka.routing.{RoundRobinPool}
import org.andrii.domaine.entitys.{SearchTask, JobsLoadTask}
import org.andrii.domaine.exceptions.MessageTypeMismatchException
import org.andrii.domaine.providers.types.BaseProviderType

/**
  * Created by andrii on 3/2/16.
  */
trait BaseProviderTrait {
  def poolSize: Int

  def providerType: BaseProviderType

  def props(provider: ActorRef): Props
}


abstract class BaseProvider extends Actor with BaseProviderTrait {

  val router =
    context.actorOf(RoundRobinPool(poolSize).props(props(self)), "router")

  def receive = {
    case searchTask: SearchTask =>
      val parseTask = JobsLoadTask(providerType, searchTask)
      val senderRef = sender()

      router.tell(parseTask, senderRef)
    case _ =>
      sender() ! MessageTypeMismatchException(sender(), self)
  }
}
