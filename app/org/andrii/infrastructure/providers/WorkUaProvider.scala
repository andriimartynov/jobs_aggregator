package org.andrii.infrastructure.providers

import akka.actor.ActorRef
import org.andrii.domaine.providers.types.WorkUaProviderType
import org.andrii.infrastructure.page_parsers.JobsLoader

/**
  * Created by andrii on 3/2/16.
  */
trait WorkUaProviderTrait extends BaseProviderTrait {
  def poolSize = 10

  def providerType = WorkUaProviderType

  def props(provider: ActorRef) = JobsLoader.props(provider)
}


class WorkUaProvider extends BaseProvider with WorkUaProviderTrait