package org.andrii.infrastructure.providers

import akka.actor.ActorRef
import org.andrii.domaine.providers.types.DouProviderType
import org.andrii.infrastructure.page_parsers.JobsLoader

/**
  * Created by andrii on 3/2/16.
  */
trait DouProviderTrait extends BaseProviderTrait {
  def poolSize = 10

  def providerType = DouProviderType

  def props(provider: ActorRef) = JobsLoader.props(provider)
}


class DouProvider extends BaseProvider with DouProviderTrait