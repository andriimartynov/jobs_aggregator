package org.andrii.infrastructure.proxys

import akka.actor.{Props, ActorRef}
import org.andrii.domaine.entitys.{JobsParsingResult, SearchResult}
import org.andrii.domaine.exceptions.MessageTypeMismatchException

/**
  * Created by andrii on 3/3/16.
  */
object ParsingProvidersProxy {
  def apply(senderRef: ActorRef) = new ParsingProvidersProxy(senderRef)

  def props(senderRef: ActorRef) = Props(ParsingProvidersProxy(senderRef))
}


class ParsingProvidersProxy(senderRef: ActorRef) extends BaseProvidersProxy(senderRef) {

  def receive = {
    case result: JobsParsingResult =>
      val searchResult = SearchResult(result.searchTask.keywords, true, Map(), result.jobs)
      senderRef ! searchResult
    case ex: Throwable =>
      senderRef ! ex
    case _ =>
      senderRef ! MessageTypeMismatchException(sender(), self)
  }
}
