package org.andrii.infrastructure.proxys

import akka.actor.{ActorRef, Actor}

/**
  * Created by andrii on 3/2/16.
  */
abstract class BaseProvidersProxy(senderRef: ActorRef) extends Actor
