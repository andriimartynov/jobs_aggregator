package org.andrii.infrastructure.page_parsers

import akka.actor.{Actor, ActorRef, Props}
import com.ning.http.client.AsyncHttpClientConfig.Builder
import org.andrii.domaine.entitys.{HttpRequestParams, HttpResponse, JobsLoadTask}
import org.andrii.domaine.exceptions.{MessageTypeMismatchException, UrlBuilderNotExistException}
import org.andrii.domaine.http_components.UrlBuilderFactory
import org.andrii.infrastructure.html_parsers.JobsParser
import play.api.libs.ws.WSClient
import play.api.libs.ws.ning.NingWSClient

/**
  * Created by andrii on 3/3/16.
  */
trait JobsLoaderTrait {
  def httpClient: WSClient = new NingWSClient(
    new Builder()
      .setRequestTimeout(3000)
      .setMaxRequestRetry(1)
      .build())

  def props(provider: ActorRef) = JobsParser.props(provider)

  def urlBuilderFactory = new UrlBuilderFactory
}

object JobsLoader {
  def apply(provider: ActorRef) = new JobsLoader(provider)

  def props(provider: ActorRef) = Props(JobsLoader(provider))
}

class JobsLoader(provider: ActorRef) extends Actor with JobsLoaderTrait {
  import akka.pattern.pipe
  import context.dispatcher

  val jobsParser = context.actorOf(props(provider))

  private def executeLoadTask(jobsLoadTask: JobsLoadTask) = {
    val option = getUrlBuilder(jobsLoadTask)

    option match {
      case Some(urlBuilder) =>
        val httpRequestParams = urlBuilder.build(jobsLoadTask.searchTask)

        loadPage(httpRequestParams, jobsLoadTask)
      case None =>
        sender() ! UrlBuilderNotExistException(jobsLoadTask)
    }
  }

  private def getUrlBuilder(jobsLoadTask: JobsLoadTask) = {
    urlBuilderFactory.getUrlBuilder(jobsLoadTask)
  }

  private def loadPage(httpRequestParams: HttpRequestParams, jobsLoadTask: JobsLoadTask) = {
    val senderRef = sender()

    val request = httpClient.url(httpRequestParams.url)
    request
      .withQueryString(httpRequestParams.queryMap: _*)
      .withFollowRedirects(true)
      .get
      .map(response => HttpResponse(jobsLoadTask, response)).recover {
      case ex: Throwable => senderRef ! ex
    }.pipeTo(jobsParser)(senderRef)
  }

  override def postStop() = {
    httpClient.close()
  }

  def receive = {
    case jobsLoadTask: JobsLoadTask =>
      executeLoadTask(jobsLoadTask)
    case _ =>
      sender() ! MessageTypeMismatchException(sender(), self)
  }
}
