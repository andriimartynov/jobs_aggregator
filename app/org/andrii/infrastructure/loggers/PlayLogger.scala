package org.andrii.infrastructure.loggers

import org.andrii.domaine.loggers.BaseLogger
import play.api.Logger

/**
  * Created by andrii on 3/18/16.
  */
trait PlayLogger extends BaseLogger {
  def logger = Logger

  def trace(message: String): Unit = {
    logger.trace(message)
  }

  def trace(message: String, error: Throwable): Unit = {
    logger.trace(message, error)
  }

  def debug(message: String): Unit = {
    logger.debug(message)
  }

  def debug(message: String, error: Throwable): Unit = {
    logger.debug(message, error)
  }

  def info(message: String): Unit = {
    logger.info(message)
  }

  def info(message: String, error: Throwable): Unit = {
    logger.info(message, error)
  }

  def warn(message: String): Unit = {
    logger.warn(message)
  }

  def warn(message: String, error: Throwable): Unit = {
    logger.warn(message, error)
  }

  def error(message: String): Unit = {
    logger.error(message)
  }

  def error(message: String, error: Throwable): Unit = {
    logger.error(message, error)
  }
}
