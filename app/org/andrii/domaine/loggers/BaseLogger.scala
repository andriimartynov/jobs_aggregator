package org.andrii.domaine.loggers

/**
  * Created by andrii on 3/18/16.
  */
trait BaseLogger {
  def trace(message: String): Unit

  def trace(message: String, error: Throwable): Unit

  def debug(message: String): Unit

  def debug(message: String, error: Throwable): Unit

  def info(message: String): Unit

  def info(message: String, error: Throwable): Unit

  def warn(message: String): Unit

  def warn(message: String, error: Throwable): Unit

  def error(message: String): Unit

  def error(message: String, error: Throwable): Unit
}
