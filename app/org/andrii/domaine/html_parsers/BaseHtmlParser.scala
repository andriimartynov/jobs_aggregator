package org.andrii.domaine.html_parsers

import org.andrii.domaine.entitys.{PageParsingResult, HttpResponse}

/**
  * Created by andrii on 3/4/16.
  */
abstract class BaseHtmlParser {
  def parse(httpResponse: HttpResponse): PageParsingResult
}
