package org.andrii.domaine.entitys

import org.andrii.domaine.providers.types.BaseProviderType

/**
  * Created by andrii on 3/3/16.
  */
object JobsParsingResult {
  def apply(response: HttpResponse, pageParsingResult: PageParsingResult) =
    new JobsParsingResult(response.provider, response.searchTask, pageParsingResult.jobs)
}

case class JobsParsingResult(provider: BaseProviderType, searchTask: SearchTask, jobs: List[Job])
