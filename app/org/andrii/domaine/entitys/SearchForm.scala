package org.andrii.domaine.entitys

import play.api.data.Form
import play.api.data.Forms._

/**
  * Created by andrii on 3/11/16.
  */
object SearchForm {

  def minLength = 3

  def getErrorsMessages(errorForm: Form[SearchForm], messages: (String, Seq[Any]) => String): Map[String, String] = errorForm
    .errors
    .map(formError => (formError.key, messages(errorForm.error(formError.key).get.message, errorForm.error(formError.key).get.args)))
    .toMap

  val form = Form(
    mapping(
      "keywords" -> text(minLength = minLength)
    )(SearchForm.apply)(SearchForm.unapply)
  )
}

case class SearchForm(keywords: String)
