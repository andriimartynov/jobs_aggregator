package org.andrii.domaine.entitys

import play.api.libs.json.Json
import play.api.mvc.WebSocket.FrameFormatter

/**
  * Created by andrii on 3/3/16.
  */
object Job {
  implicit val jobFormat = Json.format[Job]
  implicit val jobFrameFormatter = FrameFormatter.jsonFrame[Job]
}

case class Job(title: String, url: String, city: String, description: String)
