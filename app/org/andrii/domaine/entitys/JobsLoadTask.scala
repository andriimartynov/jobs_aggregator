package org.andrii.domaine.entitys

import org.andrii.domaine.providers.types.BaseProviderType

/**
  * Created by andrii on 3/3/16.
  */
object JobsLoadTask

case class JobsLoadTask(provider: BaseProviderType, searchTask: SearchTask)
