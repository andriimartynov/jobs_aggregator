package org.andrii.domaine.entitys

import play.api.libs.json.Json
import play.api.libs.json.Json._
import play.api.mvc.WebSocket.FrameFormatter


/**
  * Created by andrii on 3/12/16.
  */
object SearchResult {
  implicit val searchResultFormat = Json.format[SearchResult]
  implicit val searchResultFrameFormatter = FrameFormatter.jsonFrame[SearchResult]
}


case class SearchResult(keywords: String, isSuccess: Boolean, errorsMessages: Map[String, String], searchResults: List[Job])
