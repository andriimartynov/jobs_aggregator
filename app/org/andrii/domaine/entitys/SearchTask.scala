package org.andrii.domaine.entitys

/**
  * Created by andrii on 3/2/16.
  */
object SearchTask {
  def apply(keywords: String) = new SearchTask(keywords, 1)

  def apply(form: SearchForm) = new SearchTask(form.keywords, 1)
}

case class SearchTask(keywords: String, page: Int)
