package org.andrii.domaine.entitys

/**
  * Created by andrii on 3/15/16.
  */

object PageParsingResult

case class PageParsingResult(pages: Int, jobs: List[Job])
