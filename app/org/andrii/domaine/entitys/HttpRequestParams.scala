package org.andrii.domaine.entitys

/**
  * Created by andrii on 3/3/16.
  */
object HttpRequestParams {
  def apply(url: String) = new HttpRequestParams(url, Seq[(String, String)]())
}

case class HttpRequestParams(url: String, queryMap: Seq[(String, String)])
