package org.andrii.domaine.entitys

import org.andrii.domaine.providers.types.BaseProviderType
import play.api.libs.ws.WSResponse

/**
  * Created by andrii on 3/4/16.
  */
object HttpResponse {
  def apply(jobsLoadTask: JobsLoadTask, response: WSResponse) =
    new HttpResponse(jobsLoadTask.provider, jobsLoadTask.searchTask, response.status, response.body)
}

case class HttpResponse(provider: BaseProviderType, searchTask: SearchTask, status: Int, body: String) {
  def isSuccsess(): Boolean = {
    status == 200
  }
}