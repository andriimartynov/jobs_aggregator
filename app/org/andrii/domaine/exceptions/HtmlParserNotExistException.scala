package org.andrii.domaine.exceptions

import org.andrii.domaine.entitys.HttpResponse

/**
  * Created by andrii on 3/4/16.
  */
class HtmlParserNotExistException(message: String) extends RuntimeException(message)

object HtmlParserNotExistException {
  def apply(message: String) = new HtmlParserNotExistException(message)

  def apply(response: HttpResponse) = {
    val builder = StringBuilder.newBuilder
    builder.append("HtmlParserNotExistException: ")
    builder.append("type ")
    builder.append(response.provider.name)

    val message = builder.toString()

    new HtmlParserNotExistException(message)
  }
}