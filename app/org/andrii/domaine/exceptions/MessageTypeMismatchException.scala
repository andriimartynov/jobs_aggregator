package org.andrii.domaine.exceptions

import akka.actor.ActorRef

/**
  * Created by andrii on 3/2/16.
  */
class MessageTypeMismatchException(message: String) extends RuntimeException(message)

object MessageTypeMismatchException {
  def apply(message: String) = new MessageTypeMismatchException(message)

  def apply(from: ActorRef, to: ActorRef) = {
    val builder = StringBuilder.newBuilder
    builder.append("MessageTypeMismatchException: ")
    builder.append("from ")
    builder.append(from.path.toString)
    builder.append(", to ")
    builder.append(to.path.toString)
    val message = builder.toString()

    new MessageTypeMismatchException(message)
  }
}
