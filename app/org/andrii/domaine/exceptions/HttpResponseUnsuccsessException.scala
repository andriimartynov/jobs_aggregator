package org.andrii.domaine.exceptions

import org.andrii.domaine.entitys.HttpResponse

/**
  * Created by andrii on 3/17/16.
  */
class HttpResponseUnsuccsessException(message: String) extends RuntimeException(message)

object HttpResponseUnsuccsessException {
  def apply(message: String) = new HttpResponseUnsuccsessException(message)

  def apply(response: HttpResponse) = {
    val builder = StringBuilder.newBuilder
    builder.append("HttpResponseUnsuccsessException: ")
    builder.append("provider ")
    builder.append(response.provider.name)
    builder.append("status ")
    builder.append(response.status)
    builder.append("keywords ")
    builder.append(response.searchTask.keywords)
    builder.append("page ")
    builder.append(response.searchTask.page)

    val message = builder.toString()

    new HttpResponseUnsuccsessException(message)
  }
}
