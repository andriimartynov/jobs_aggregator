package org.andrii.domaine.exceptions

import org.andrii.domaine.entitys.JobsLoadTask

/**
  * Created by andrii on 3/4/16.
  */
class UrlBuilderNotExistException(message: String) extends RuntimeException(message)

object UrlBuilderNotExistException {
  def apply(message: String) = new UrlBuilderNotExistException(message)

  def apply(jobsLoadTask: JobsLoadTask) = {
    val builder = StringBuilder.newBuilder
    builder.append("UrlBuilderNotExistException: ")
    builder.append("type ")
    builder.append(jobsLoadTask.provider.name)

    val message = builder.toString()

    new UrlBuilderNotExistException(message)
  }
}