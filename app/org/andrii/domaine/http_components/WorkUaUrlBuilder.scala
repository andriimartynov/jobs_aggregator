package org.andrii.domaine.http_components

import org.andrii.domaine.entitys.{HttpRequestParams, SearchTask}
import views.html.helper

/**
  * Created by andrii on 3/3/16.
  */
object WorkUaUrlBuilder {
  def apply() = new WorkUaUrlBuilder
}

class WorkUaUrlBuilder extends BaseUrlBuilder {
  val baseUrl = "http://www.work.ua/jobs-"

  private def buildUrl(searchTask: SearchTask): String = {
    baseUrl + helper.urlEncode(searchTask.keywords)
  }

  private def buildQueryMap(searchTask: SearchTask): Seq[(String, String)] = {
    Seq(("page", searchTask.page.toString))
  }

  override def build(searchTask: SearchTask) = {
    val url = buildUrl(searchTask)
    val queryMap = buildQueryMap(searchTask)
    HttpRequestParams(url, queryMap)
  }
}
