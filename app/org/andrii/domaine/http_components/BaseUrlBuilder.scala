package org.andrii.domaine.http_components

import org.andrii.domaine.entitys.{HttpRequestParams, SearchTask}
import views.html.helper

/**
  * Created by andrii on 3/3/16.
  */
abstract class BaseUrlBuilder {
  def baseUrl: String

  def build(SearchTask: SearchTask) = {
    val url = baseUrl + helper.urlEncode(SearchTask.keywords)
    HttpRequestParams(url)
  }
}
