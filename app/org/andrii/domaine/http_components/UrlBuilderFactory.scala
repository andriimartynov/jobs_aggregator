package org.andrii.domaine.http_components

import org.andrii.domaine.entitys.JobsLoadTask
import org.andrii.domaine.providers.types.{WorkUaProviderType, DouProviderType}

/**
  * Created by andrii on 3/4/16.
  */
class UrlBuilderFactory {
  val douUrlBuilder = DouUrlBuilder()
  val workUaUrlBuilder = WorkUaUrlBuilder()

  def getUrlBuilder(jobsLoadTask: JobsLoadTask): Option[BaseUrlBuilder] = {
    jobsLoadTask.provider match {
      case DouProviderType => Some(douUrlBuilder)
      case WorkUaProviderType => Some(workUaUrlBuilder)
      case _ => None
    }
  }
}
