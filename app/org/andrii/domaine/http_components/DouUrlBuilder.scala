package org.andrii.domaine.http_components

import org.andrii.domaine.entitys.{HttpRequestParams, SearchTask}

/**
  * Created by andrii on 3/3/16.
  */
object DouUrlBuilder {
  def apply() = new DouUrlBuilder
}

class DouUrlBuilder extends BaseUrlBuilder {
  val baseUrl = "http://jobs.dou.ua/vacancies/"

  override def build(SearchTask: SearchTask) = {
    val url = baseUrl
    val queryMap = Seq(("search", SearchTask.keywords))
    HttpRequestParams(url, queryMap)
  }
}
