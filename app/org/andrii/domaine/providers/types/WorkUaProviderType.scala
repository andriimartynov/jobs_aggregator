package org.andrii.domaine.providers.types

/**
  * Created by andrii on 3/2/16.
  */
object WorkUaProviderType extends BaseProviderType {
  def name: String = "work_ua"
}
