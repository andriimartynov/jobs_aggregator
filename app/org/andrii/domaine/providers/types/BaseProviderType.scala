package org.andrii.domaine.providers.types

/**
  * Created by andrii on 3/2/16.
  */
abstract class BaseProviderType {
  def name: String
}
