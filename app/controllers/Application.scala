package controllers

import akka.actor.ActorSystem
import com.google.inject.Inject
import org.andrii.domaine.entitys.{SearchResult, SearchForm}
import org.andrii.domaine.entitys.SearchResult._
import org.andrii.infrastructure.routers.JobsSearchRouter
import org.andrii.infrastructure.web_sockets.SearchWebSocketActor
import play.api.Play.current
import play.api.mvc._
import play.api.libs.json._
import javax.inject.Singleton

@Singleton
class Application @Inject()(system: ActorSystem) extends Controller {
  val jobSearchRouter =
    system.actorOf(JobsSearchRouter.props, JobsSearchRouter.name)

  def index = Action { implicit request => {
    val keywords = request.queryString.getOrElse("keywords", Seq[String]("")).mkString
    val keywordsMinLength = SearchForm.minLength.toString
    val webSocketURL = routes.Application.wsSearch().webSocketURL()
    val configMap = Map[String, String](
      "keywords" -> keywords,
      "keywordsMinLength" -> keywordsMinLength,
      "webSocketURL" -> webSocketURL
    )

    val config = Json.toJson(configMap).toString()
    Ok(views.html.index(config))
  }
  }

  def wsSearch = WebSocket.acceptWithActor[JsValue, SearchResult] {
    request =>
      out => {
        SearchWebSocketActor.props(out, jobSearchRouter)
      }
  }
}
