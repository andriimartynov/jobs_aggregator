require.config({
    baseUrl: "/assets/javascripts",
    paths: {
        'react': '../lib/react/react',
        'react-dom': '../lib/react/react-dom',
        'App': 'components/App',
        'BrowserHistory': 'components/BrowserHistory',
        'BrowserNotSupported': 'components/BrowserNotSupported',
        'SearchErrors': 'components/SearchErrors',
        'SearchForm': 'components/SearchForm',
        'SearchFormPage': 'components/SearchFormPage',
        'SearchPage': 'components/SearchPage',
        'SearchResults': 'components/SearchResults',
        'WS': 'components/WS',
        'WSError': 'components/WSError',
        'WSMessages': 'components/WSMessages'
    }
});

require(['react', 'react-dom', "components/App"], function (React, ReactDOM, App) {
    'use strict';

    ReactDOM.render(
        <App />,
        document.getElementById('content')
    );
});