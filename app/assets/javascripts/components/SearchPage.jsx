define(function (require) {
    var React = require('react');
    var SearchForm = require('SearchForm');
    var SearchErrors = require('SearchErrors');
    var SearchResults = require('SearchResults');


    return React.createClass({
        componentDidMount: function () {
            this.setState({
                keywords: this.props.keywords,
                searchData: this.props.searchData
            });
        },
        componentWillReceiveProps: function (nextProps) {
            this.setState({
                keywords: nextProps.keywords,
                searchData: nextProps.searchData
            });
        },
        getInitialState: function () {
            return {
                searchData: {
                    "isSuccess": true,
                    "errorsMessages": {},
                    "searchResults": []
                }
            };
        },
        handleKeywordsChange: function (keywords) {
            this.setState({
                keywords: keywords
            });
            this.props.onKeywordsChange(keywords);
        },
        handleKeywordsSubmit: function (keywords) {
            this.props.onKeywordsSubmit(keywords);
        },
        render: function () {
            var content;
            if (this.state.searchData.isSuccess) {
                content = <SearchResults searchResults={this.state.searchData.searchResults}/>;
            } else {
                content = <SearchErrors errorsMessages={this.state.searchData.errorsMessages}/>;
            }

            return (
                <div>
                    <nav className="navbar navbar-inverse">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                        data-toggle="collapse" className="navbar-toggle collapsed" type="button">
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <a href="#" className="navbar-brand">Jobs Aggregator</a>
                            </div>
                            <div className="collapse navbar-collapse" id="navbar">
                                <div className="col-md-5 nav navbar-nav navbar-right navbar-form">
                                    <SearchForm keywords={this.state.keywords}
                                                onKeywordsChange={this.handleKeywordsChange}
                                                onKeywordsSubmit={this.handleKeywordsSubmit}/>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div className="col-md-12">
                        {content}
                    </div>
                </div>
            );
        }
    });
});