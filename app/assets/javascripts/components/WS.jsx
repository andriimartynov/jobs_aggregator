define(function (require) {
    var connection;
    var firstTimeononopenHandler;
    var isFirstTimeConnectionOpened = true;
    var isConnectionClosed = false;
    var onerrorHandler;
    var onmessageHandler;
    var reconectTimeout = 5000;
    var timerID = 0;


    var WS = {};

    function onclose(url, callback) {
        connection.onclose = function (event) {
            if (!isConnectionClosed && !timerID) {
                timerID = setInterval(function () {
                    WS.connect(url, callback);
                }, reconectTimeout);
            }
        };
    }

    function onopen() {
        connection.onopen = function (event) {
            if (isFirstTimeConnectionOpened && firstTimeononopenHandler) {
                firstTimeononopenHandler();
            }
            isFirstTimeConnectionOpened = false;
            if (onerrorHandler) {
                connection.onerror = onerrorHandler;
            }
            if (onmessageHandler) {
                connection.onmessage = onmessageHandler;
            }
            if (timerID) {
                window.clearInterval(timerID);
                timerID = 0;
            }
        };
    }

    WS.close = function (callback) {
        isConnectionClosed = true;
        if (!connection) {
            var e = new Error('Connection is not exist.');
            return callback(e);
        }

        connection.close();
        callback(null);
    };

    WS.connect = function (url, callback) {
        try {
            connection = new WebSocket(url);
            onclose(url, callback);
            onopen();

            callback(null);
        } catch (e) {
            callback(e);
        }
    };

    WS.onerror = function (handler) {
        onerrorHandler = handler;
    };

    WS.onmessage = function (handler) {
        onmessageHandler = handler;
    };

    WS.onopen = function (handler) {
        firstTimeononopenHandler = handler;
    };

    WS.send = function (msg, callback) {
        if (!connection) {
            var e = new Error('Connection is not exist.');
            return callback(e);
        }
        connection.send(msg);
    };

    return WS;
});