define(function (require) {
    var React = require('react');

    return React.createClass({
        render: function () {
            return (
                <div className="alert alert-danger" role="alert">Oh no! Your browser is not supported.</div>
            );
        }
    });
});