define(function (require) {
    var React = require('react');

    return React.createClass({
        render: function () {
            var errorsMessages = this.props.errorsMessages;

            return (
                <div>
                    {errorsMessages &&  Object.keys(errorsMessages).map(function(key) {
                        return <div className="alert alert-danger" role="alert" key={key}>{key} - {errorsMessages[key]}</div>;
                    })}
                </div>
            );
        }
    });
});