define(function (require) {
    var browserHistory = {};

    browserHistory.getSate = function () {
        return history.state;
    };

    browserHistory.pushState = function (state) {
        if (state && state.hasOwnProperty("keywords")) {
            var url = "/?keywords=" + decodeURIComponent(state.keywords);
            history.pushState(state, "", url);
        }
    };

    browserHistory.replaceState = function (state) {
        if (state && state.hasOwnProperty("keywords")) {
            var url = "/?keywords=" + decodeURIComponent(state.keywords);
            history.replaceState(state, "", url);
        }
    };

    browserHistory.onPopState = function (callback) {
        window.onpopstate = callback;
    };
    
    return browserHistory;
});