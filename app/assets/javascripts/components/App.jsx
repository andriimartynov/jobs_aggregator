define(function (require) {
    var React = require('react');
    var BrowserHistory = require('BrowserHistory');
    var BrowserNotSupported = require('BrowserNotSupported');
    var SearchFormPage = require('SearchFormPage');
    var SearchPage = require('SearchPage');
    var WS = require('WS');
    var WSError = require('WSError');
    var WSMessages = require('WSMessages');
    var onFormPage = true;

    return React.createClass({
        componentDidMount: function () {
            var config = this.state.config;
            this.connectWS(config.webSocketURL);

            this.setState({
                keywords: config.keywords
            });
        },
        componentWillUnmount: function () {
            WS.close(function (e) {
                this.handleErrors(e);
            });
        },
        connectWS: function (url) {
            var self = this;
            var callback = function (e) {
                self.handleErrors(e);
            };

            WS.connect(url, callback);
            WS.onerror(this.onWSError);
            WS.onmessage(this.onWSMessage);
            WS.onopen(this.onWSFirstTimeOpen);
        },
        getConfig: function () {
            var config = {};
            try {
                var contentElemnt = document.getElementById('content');
                var configStr = contentElemnt.dataset.config;
                config = JSON.parse(configStr);
            } catch (e) {
                this.handleErrors(e);
                config.keywords = "";
            }
            return config;
        },
        getInitialState: function () {
            var config = this.getConfig();
            var state = {
                keywords: config.keywords,
                searchData: {
                    "isSuccess": true,
                    "errorsMessages": {},
                    "searchResults": []
                }
            };
            BrowserHistory.replaceState(state);
            BrowserHistory.onPopState(this.onPopState);

            return {
                config: config,
                searchData: {
                    "isSuccess": true,
                    "errorsMessages": {},
                    "searchResults": []
                },
                keywords: '',
                hasWSError: false
            };
        },
        isBrowserSupported: function () {
            return window && window.WebSocket && window.history;
        },
        handleErrors: function (e) {
            //TODO: errors handler
            if (e && console) {
                console.log(e.name + ': ' + e.message);
            }
        },
        handleKeywordsChange: function (keywords) {
            this.setState({
                keywords: keywords
            });
        },
        handleKeywordsSubmit: function (keywords) {
            this.setState({
                keywords: keywords,
                searchData: {
                    "isSuccess": true,
                    "errorsMessages": {},
                    "searchResults": []
                }
            });
            BrowserHistory.pushState(this.state);
            this.sendWSMessage(keywords);
        },
        onPopState: function () {
            var state = BrowserHistory.getSate();
            this.setState(
                state
            );
        },
        onWSFirstTimeOpen: function (e) {
            var keywords = this.state.keywords;
            if (keywords && keywords.length) {
                this.sendWSMessage(keywords);
            }
        },
        onWSError: function (e) {
            this.setState({
                hasWSError: true
            });
        },
        onWSMessage: function (event) {
            var parsed = WSMessages.parse(event);
            if (parsed) {
                var searchData = this.state.searchData;
                searchData.isSuccess = parsed.isSuccess;
                searchData.errorsMessages = parsed.errorsMessages;

                if (parsed.keywords === this.state.keywords) {
                    var searchResults = this.state.searchData.searchResults.concat(parsed.searchResults);
                    searchData.searchResults = searchResults;
                }
                this.setState({
                    searchData: searchData
                });
                BrowserHistory.replaceState(this.state);
            }
        },
        render: function () {
            if (!this.isBrowserSupported()) {
                return (
                    <BrowserNotSupported />
                );
            }
            if (this.state.hasWSError) {
                return (
                    <WSError />
                );
            }
            if (!onFormPage || (this.state.keywords && this.state.keywords.length)) {
                onFormPage = false;
                return (
                    <SearchPage onKeywordsChange={this.handleKeywordsChange}
                                onKeywordsSubmit={this.handleKeywordsSubmit}
                                searchData={this.state.searchData}
                                keywords={this.state.keywords}/>
                );
            }
            return (
                <SearchFormPage onKeywordsChange={this.handleKeywordsChange} keywords={this.state.keywords}/>
            );
        },
        sendWSMessage: function (keywords) {
            var self = this;
            var callback = function (e) {
                self.handleErrors(e);
            };

            var msg = WSMessages.build(keywords);
            WS.send(msg, callback);
        }
    });
});