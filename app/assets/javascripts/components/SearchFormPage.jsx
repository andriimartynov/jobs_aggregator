define(function (require) {
    var React = require('react');
    var SearchForm = require('SearchForm');

    return React.createClass({
        getInitialState: function () {
            return {};
        },
        handleKeywordsChange: function (keywords) {
            this.props.onKeywordsChange(keywords);
        },
        handleKeywordsSubmit: function (keywords) {
            return;
        },
        render: function () {
            return (
                <div className="col-md-5 col-md-offset-3 margin-top-150 form-horizontal">
                    <SearchForm keywords={this.props.keywords} onKeywordsChange={this.handleKeywordsChange}
                                onKeywordsSubmit={this.handleKeywordsSubmit}/>
                </div>
            );
        }
    });
});