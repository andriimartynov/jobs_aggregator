define(function (require) {
    var WSMessages = {};
    var message = {
        keywords: ''
    };

    WSMessages.build = function (keywords) {
        message.keywords = keywords;
        return JSON.stringify(message);
    };

    WSMessages.parse = function (event) {
        if (event && event.data) {
            try {
                return JSON.parse(event.data);
            } catch (e) {
                return null;
            }
        }
    };

    return WSMessages;
});