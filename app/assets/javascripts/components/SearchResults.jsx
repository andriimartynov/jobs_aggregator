define(function (require) {
    var React = require('react');
    var step = 10;

    return React.createClass({
        getInitialState: function () {
            return {
                maxItemsOnPage: step
            };
        },
        componentWillReceiveProps: function (nextProps) {
            if (nextProps.searchResults && nextProps.searchResults.length === 0) {
                this.setState({
                    maxItemsOnPage: step
                });
            }
        },
        showMoreHandler: function () {
            var newMaxItemsOnPage = step + this.state.maxItemsOnPage;
            this.setState({
                maxItemsOnPage: newMaxItemsOnPage
            });
        },
        render: function () {
            var searchResults = this.props.searchResults;
            var maxItemsOnPage = this.state.maxItemsOnPage;
            var i = 0;
            var showMore;

            if (searchResults.length > maxItemsOnPage) {
                showMore =
                    <button type="submit" className="btn btn-default" onClick={this.showMoreHandler}>Show More</button>
            } else {
                showMore = <div></div>
            }

            return (
                <div>
                    <table className="table table-striped">
                        <tbody>
                        {searchResults && searchResults.map(function (job) {
                            if (i < maxItemsOnPage) {
                                i++;
                                return <tr key={job.title + i}>
                                    <td>
                                        <h4>
                                            <div>
                                                <a href={job.url}>
                                                    {job.title}
                                                </a>
                                            </div>
                                            <small>{job.city}</small>
                                        </h4>
                                        <div>
                                            {job.description}
                                        </div>
                                    </td>
                                </tr>
                            }
                        })}
                        </tbody>
                    </table>
                    {showMore}
                </div>
            );
        }
    });
});