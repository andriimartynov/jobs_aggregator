define(function (require) {
    var React = require('react');
    var ReactDOM = require('react-dom');

    return React.createClass({
        componentDidMount: function () {
            ReactDOM.findDOMNode(this.refs.keywords).focus();
        },
        getInitialState: function () {
            return {};
        },
        handleChange: function (e) {
            var keywords = e.target.value;
            this.props.onKeywordsChange(keywords);
        },
        handleSubmit: function (e) {
            e.preventDefault();
            var keywords = e.target.elements[0].value;
            this.props.onKeywordsSubmit(keywords);
            return false;
        },
        render: function () {
            return (
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label hrmlFor="keywords" className="col-sm-2 control-label">Keywords</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   className="form-control"
                                   id="keywords"
                                   ref="keywords"
                                   placeholder="Scala Developer"
                                   onChange={this.handleChange}
                                   value={this.props.keywords}
                                   autofocus
                            ></input>
                        </div>
                    </div>
                </form>
            );
        }
    });
});