define(function (require) {
    var React = require('react');

    return React.createClass({
        render: function () {
            return (
                <div className="alert alert-danger" role="alert">Error: Oops. Something went wrong. Please try again
                    later.</div>
            );
        }
    });
});